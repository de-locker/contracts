// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

import {Locker} from "./Locker.sol";

contract LockerFactory {
    event NewLocker(address indexed owner, address indexed locker);

    function newLocker(string memory _name, string memory _symbol) public {
        Locker locker = new Locker(msg.sender, _name, _symbol);
        emit NewLocker(msg.sender, address(locker));
    }
}
