import "@nomicfoundation/hardhat-toolbox-viem";
import "@nomicfoundation/hardhat-verify";
import { toHex, toBytes } from "viem";

import { HardhatUserConfig, vars } from "hardhat/config";

const NODEREAL_API_KEY = vars.get("NODEREAL_API_KEY");

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.24",
    settings: {
      optimizer: {
        enabled: true,
      },
    },
  },
  networks: {
    hardhat: {},
    opbnbTestnet: {
      url: "https://opbnb-testnet-rpc.bnbchain.org",
      chainId: 5611,
      accounts: [vars.get("ACCOUNT_PRIV_KEY")],
    },
  },
  ignition: {
    strategyConfig: {
      create2: {
        salt: toHex(toBytes("v1.0.0", { size: 32 })),
      },
    },
  },
  etherscan: {
    apiKey: {
      opbnbTestnet: NODEREAL_API_KEY,
    },
    customChains: [
      {
        network: "opbnbTestnet",
        chainId: 5611,
        urls: {
          apiURL: `https://open-platform.nodereal.io/${NODEREAL_API_KEY}/op-bnb-testnet/contract/`,
          browserURL: "https://testnet.opbnbscan.com/",
        },
      },
    ],
  },
};

export default config;
