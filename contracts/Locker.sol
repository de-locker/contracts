// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

import {AccessControl} from "@openzeppelin/contracts/access/AccessControl.sol";
import {ERC721, ERC721URIStorage} from "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import {ECDSA} from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import {MessageHashUtils} from "@openzeppelin/contracts/utils/cryptography/MessageHashUtils.sol";

contract Locker is ERC721URIStorage, AccessControl {
    using ECDSA for bytes32;
    using MessageHashUtils for bytes32;

    bytes32 public constant GUEST_ROLE = keccak256("GUEST_ROLE");

    mapping(uint256 tokenId => uint256) private _availableAt;
    mapping(uint256 tokenId => uint256) private _expiredAt;

    constructor(
        address _owner,
        string memory _name,
        string memory _symbol
    ) ERC721(_name, _symbol) {
        _grantRole(DEFAULT_ADMIN_ROLE, _owner);
    }

    modifier onlyOwner(uint256 tokenId) {
        require(_ownerOf(tokenId) == msg.sender, "!owner");
        _;
    }

    modifier onlyAvailable(uint256 tokenId) {
        require(_isAvailable(tokenId), "!available");
        _;
    }

    function _isAvailable(uint256 tokenId) internal view returns (bool) {
        return
            _availableAt[tokenId] <= block.timestamp &&
            block.timestamp < _expiredAt[tokenId];
    }

    function _verify(
        bytes32 data,
        bytes memory signature,
        address account
    ) internal pure returns (bool) {
        return data.toEthSignedMessageHash().recover(signature) == account;
    }

    function supportsInterface(
        bytes4 interfaceId
    )
        public
        view
        virtual
        override(ERC721URIStorage, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function safeMint(
        address to,
        uint256 tokenId,
        string memory uri,
        uint256 availableAt,
        uint256 expiredAt
    ) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
        _availableAt[tokenId] = availableAt;
        _expiredAt[tokenId] = expiredAt;
    }

    function burn(uint256 tokenId) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _burn(tokenId);
    }

    function checkedIn(
        bytes32 data,
        bytes memory signature,
        uint256 tokenId
    ) public view onlyOwner(tokenId) onlyAvailable(tokenId) returns (bool) {
        return _verify(data, signature, msg.sender);
    }

    function isAvailable(uint256 tokenId) public view returns (bool) {
        return _isAvailable(tokenId);
    }
}
