import { buildModule } from "@nomicfoundation/hardhat-ignition/modules";

const LockerFactoryModule = buildModule("LockerFactoryModule", (m) => {
  const lockerFactory = m.contract("LockerFactory");

  return { lockerFactory };
});

export default LockerFactoryModule;
